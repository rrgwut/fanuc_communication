
/* 
 * File:   FanucCommunication.h
 * Authors: Wojchech Mormul, Maciej Przybylski
 *
 * Created on 11 maja 2016, 14:37
 */

#ifndef FANUC_COMMUNICATION_POSEARRAYSENDER_H
#define FANUC_COMMUNICATION_POSEARRAYSENDER_H

#include <string>
#include <thread>
#include <mutex>

#include <boost/asio.hpp>

#include <ros/ros.h>
#include <tf/tf.h>

#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseArray.h>

namespace fanuc_communication{

class PoseArrayServer {
public:
    explicit PoseArrayServer();
    
    virtual ~PoseArrayServer();
    
    
private:
    ros::NodeHandle nh_;
    
    std::string user_frame_id_;
    int max_number_of_points_;
    
    std::vector<double> workspace_lower_bound_; //[x,y,z,w,p,r] [mm,mm,mm,deg,deg,deg]
    std::vector<double> workspace_upper_bound_; //[x,y,z,w,p,r] [mm,mm,mm,deg,deg,deg]

    geometry_msgs::PoseArray pose_array_;
    std::mutex pose_array_mutex_;
    
    ros::Subscriber sub_pose_array_;
    ros::Publisher pub_pose_array_checked_;
    
    ///communication
    int port_;
    bool communication_loop_ok_;
    std::mutex communication_loop_ok_mutex_;
    std::thread *communication_loop_thread_{nullptr};
    
    
    
    void poseArrayCallback(geometry_msgs::PoseArray const& pose_array_msg);
    
    void startCommunicationThread();
    void stopCommunicationThread();
    void communicationLoop();
    
    bool communicationLoopOK();
    void setCommunicationLoopOK(bool ok);
    
    geometry_msgs::PoseArray poseArray();
    void setPoseArray(geometry_msgs::PoseArray pose_array);
    void resetPoseArray();
    
    bool checkPose(geometry_msgs::Pose pose);
    bool checkPoseArray(geometry_msgs::PoseArray pose_array);
    
    bool poseArrayRequest(boost::asio::ip::tcp::socket &socket, char command);
    void sendPoseArray(boost::asio::ip::tcp::socket &socket, geometry_msgs::PoseArray pose_array);
    void sendPose(boost::asio::ip::tcp::socket &socket, geometry_msgs::Pose pose);
};

}//namespace fanuc_communication

#endif /* FANUC_COMMUNICATION_POSEARRAYSENDER_H */


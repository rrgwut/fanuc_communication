/* 
 * File:   fanuc_communication_node.cpp
 * Authors: Wojciech Mormul, Maciej Przybylski
 *
 * Created on 11 maja 2016, 14:46
 */

#include <cstdlib>
#include "fanuc_communication/PoseArrayServer.h"


/*
 * 
 */
int main(int argc, char** argv) 
{
    ros::init(argc, argv, "fanuc_communication_node");
    
    fanuc_communication::PoseArrayServer server;
    
    ros::Rate loop_rate(20);
    
    while(ros::ok())
    {
        ros::spinOnce();
        loop_rate.sleep();
    }

    return 0;
}


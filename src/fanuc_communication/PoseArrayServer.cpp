
/* 
 * File:   FanucCommunication.cpp
 * Authors: Wojchech Mormul, Maciej Przybylski
 * 
 * Created on 11 maja 2016, 14:37
 */

#include "fanuc_communication/PoseArrayServer.h"

#include <exception>
#include <sstream>
#include <iomanip>
#include <boost/lexical_cast.hpp>

#include <angles/angles.h>

namespace fanuc_communication{

PoseArrayServer::PoseArrayServer() : nh_("~")
{   
    if(!nh_.getParam("user_frame_id",user_frame_id_)) 
        throw std::invalid_argument("user_frame_id not set");
    
    nh_.param("max_number_of_points",max_number_of_points_,4);
    
    if(!nh_.getParam("workspace_lower_bound",workspace_lower_bound_)) 
        ROS_WARN("Workspace_lower_bound not set. No pose checking");
    
    if(!nh_.getParam("workspace_upper_bound",workspace_upper_bound_)) 
        ROS_WARN("Workspace_upper_bound not set. No pose checking");
    
    nh_.param("port", port_, 59003);
        
    sub_pose_array_ = nh_.subscribe("pose_array",1,&PoseArrayServer::poseArrayCallback, this);
    pub_pose_array_checked_ = nh_.advertise<geometry_msgs::PoseArray>("pose_array_checked", 1, false);
    
    ROS_INFO("%s is running...", nh_.getNamespace().c_str());
    
    startCommunicationThread();
}

PoseArrayServer::~PoseArrayServer() 
{   
    stopCommunicationThread();
}

void PoseArrayServer::poseArrayCallback(geometry_msgs::PoseArray const& pose_array_msg)
{
    if(checkPoseArray(pose_array_msg))
    {        
        setPoseArray(pose_array_msg);
        pub_pose_array_checked_.publish(pose_array_msg);
        
        ROS_INFO("New pose array set");
    }       
    else
    {
        ROS_WARN("Pose array rejected");
    }
}

void PoseArrayServer::startCommunicationThread()
{
    assert(!communication_loop_thread_);
    setCommunicationLoopOK(true);
    communication_loop_thread_ = new std::thread(&PoseArrayServer::communicationLoop, this);
}
    
void PoseArrayServer::stopCommunicationThread()
{
    setCommunicationLoopOK(false);
    
    //we have to connect to this server to break waiting on acceptor
    try
    {
        boost::asio::io_service io_service;
        boost::asio::ip::tcp::resolver resolver(io_service);
        boost::asio::ip::tcp::resolver::query query("127.0.0.1", 
                boost::lexical_cast<std::string>(port_));
        
        boost::asio::ip::tcp::socket socket(io_service);
        boost::asio::connect(socket, resolver.resolve(query));
    }catch(std::exception& e)
    {
        
    }
    
    
    communication_loop_thread_->join();
    delete communication_loop_thread_;
    communication_loop_thread_ = nullptr;
}

void PoseArrayServer::communicationLoop()
{
    
    boost::asio::io_service io_service;
    boost::asio::ip::tcp::acceptor acceptor(io_service, 
            boost::asio::ip::tcp::endpoint(
                boost::asio::ip::tcp::v4(), port_));
    
    ROS_INFO("Server is listening on port %d", port_);

    while(communicationLoopOK())
    {
        try
        {
            
            boost::asio::ip::tcp::socket socket(io_service);
            acceptor.accept(socket);
            
            ROS_INFO("Request from %s", 
                    socket.remote_endpoint().address().to_string().c_str());

            geometry_msgs::PoseArray pose_array = poseArray();

            if(poseArrayRequest(socket, 'G'))
            {
                sendPoseArray(socket, pose_array);
            }
            
            

        }catch(std::exception& e)
        {
            ROS_ERROR("Communication exception: %s", e.what());
        }
    }
    
    ROS_INFO("Server has finished");
}

bool PoseArrayServer::communicationLoopOK()
{
    std::lock_guard<std::mutex> lock(communication_loop_ok_mutex_);
    
    return communication_loop_ok_;
}

void PoseArrayServer::setCommunicationLoopOK(bool ok)
{
    std::lock_guard<std::mutex> lock(communication_loop_ok_mutex_);
    
    communication_loop_ok_ = ok;
}

geometry_msgs::PoseArray PoseArrayServer::poseArray()
{
    std::lock_guard<std::mutex> lock(pose_array_mutex_);
    
    return pose_array_;
}
    
void PoseArrayServer::setPoseArray(geometry_msgs::PoseArray pose_array)
{
    std::lock_guard<std::mutex> lock(pose_array_mutex_);
    
    pose_array_ = pose_array;
}
    
void PoseArrayServer::resetPoseArray()
{
    setPoseArray(geometry_msgs::PoseArray());
}

bool PoseArrayServer::checkPose(geometry_msgs::Pose pose)
{
    bool ok = true;
    
    ok = ok && pose.position.x>=workspace_lower_bound_[0];
    ok = ok && pose.position.x<=workspace_upper_bound_[0];
    ok = ok && pose.position.y>=workspace_lower_bound_[1];
    ok = ok && pose.position.y<=workspace_upper_bound_[1];
    ok = ok && pose.position.z>=workspace_lower_bound_[2];
    ok = ok && pose.position.z<=workspace_upper_bound_[2];
    
    tf::Quaternion tf_orientation;
    tf::quaternionMsgToTF(pose.orientation, tf_orientation);
    
    double roll, pitch, yaw;
    tf::Matrix3x3(tf_orientation).getRPY(roll, pitch, yaw);
    
    roll = angles::to_degrees(roll);
    pitch = angles::to_degrees(pitch);
    yaw = angles::to_degrees(yaw);
    
    ok = ok && roll>=workspace_lower_bound_[3];
    ok = ok && roll<=workspace_upper_bound_[3];
    ok = ok && pitch>=workspace_lower_bound_[4];
    ok = ok && pitch<=workspace_upper_bound_[4];
    ok = ok && yaw>=workspace_lower_bound_[5];
    ok = ok && yaw<=workspace_upper_bound_[5];
    
    return ok;
}
    
bool PoseArrayServer::checkPoseArray(geometry_msgs::PoseArray pose_array)
{
    if(pose_array.header.frame_id!=user_frame_id_)
    {
        ROS_ERROR("[fanuc_communication] bad frame id! expected %s, got %s", 
                user_frame_id_.c_str(), pose_array.header.frame_id.c_str());
        return false;
    }
    
    if(workspace_lower_bound_.size()==6 && workspace_upper_bound_.size()==6)    
    for(auto pose: pose_array.poses)
    {
        if(!checkPose(pose))
        {
            return false;
        }
    }
    
    return true;
}

bool PoseArrayServer::poseArrayRequest(boost::asio::ip::tcp::socket &socket, char command)
{
    boost::array<char, 128> buf;
    boost::system::error_code error;

    size_t len = socket.read_some(boost::asio::buffer(buf), error);

    if (error == boost::asio::error::eof)
    {
      return false; // Connection closed cleanly by peer.
    }
    else if (error)
    {
      throw boost::system::system_error(error); // Some other error.
    }

    if(len!=1)
    {        
        throw std::length_error("len!=1, which is: "+boost::lexical_cast<std::string>(len));
    }
            
    return buf[0]==command;
}

void PoseArrayServer::sendPoseArray(boost::asio::ip::tcp::socket &socket, 
        geometry_msgs::PoseArray pose_array)
{
    int pose_number = std::min(static_cast<int>(pose_array.poses.size()), max_number_of_points_);
    std::string pose_number_str = boost::lexical_cast<std::string>(pose_number);
    
    boost::system::error_code error;
    boost::asio::write(socket, boost::asio::buffer(pose_number_str), error);
    
    int i=0;
    for(auto pose: pose_array.poses)
    {
        if(i>=pose_number)
            break;
        
        ++i;
        
        if(poseArrayRequest(socket, 'P'))
        {
            sendPose(socket, pose);
        }
    }
    
    ROS_INFO("Pose array sent to %s", 
                    socket.remote_endpoint().address().to_string().c_str());
    
    //resetPoseArray();
    
}

template<typename T>
std::string numberToStringOfSize8(T number)
{
    std::ostringstream stream;
    
    stream << std::fixed << std::setprecision(3) << std::setw(8)
                 << std::left <<std::setfill('0') <<  number ;
    
    return stream.str();
}

void PoseArrayServer::sendPose(boost::asio::ip::tcp::socket &socket, 
        geometry_msgs::Pose pose)
{    
    std::string pose_string;
    
    pose_string += numberToStringOfSize8(pose.position.x*1000.0);
    pose_string += numberToStringOfSize8(pose.position.y*1000.0);
    pose_string += numberToStringOfSize8(pose.position.z*1000.0);
    
    tf::Quaternion tf_orientation;
    tf::quaternionMsgToTF(pose.orientation, tf_orientation);
    
    double roll, pitch, yaw;
    tf::Matrix3x3(tf_orientation).getRPY(roll, pitch, yaw);
        
    pose_string+=numberToStringOfSize8(angles::to_degrees(roll));
    pose_string+=numberToStringOfSize8(angles::to_degrees(pitch));
    pose_string+=numberToStringOfSize8(angles::to_degrees(yaw));
    
    boost::system::error_code error;
    boost::asio::write(socket, boost::asio::buffer(pose_string), error);
}


}//namespace fanuc_communication

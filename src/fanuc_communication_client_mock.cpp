
/* 
 * File:   fanuc_communication_client_mock.cpp
 * Author: Maciej Przybylski
 *
 * Created on 12 maja 2016, 09:07
 */

#include <cstdlib>
#include <iostream>

#include <boost/asio.hpp>
#include <boost/lexical_cast.hpp>


/*
 * 
 */
int main(int argc, char** argv) 
{
    try
    {
        boost::asio::io_service io_service;
        boost::asio::ip::tcp::resolver resolver(io_service);
        boost::asio::ip::tcp::resolver::query query("127.0.0.1", 
                boost::lexical_cast<std::string>(59003));
        
        boost::asio::ip::tcp::socket socket(io_service);
        boost::asio::connect(socket, resolver.resolve(query));
        
        boost::system::error_code error;
        
        boost::array<char, 1> wbuf;
        wbuf[0] = 'G';
        boost::asio::write(socket, boost::asio::buffer(wbuf), error);
        if (error)
        {
          throw boost::system::system_error(error); // Some other error.
        }
        
        
        boost::array<char, 128> buf;        
        size_t len = socket.read_some(boost::asio::buffer(buf), error);

        if (error == boost::asio::error::eof)
        {
          return 0; 
        }
        else if (error)
        {
          throw boost::system::system_error(error); 
        }
        
        
        std::string str(buf.begin(), buf.begin()+len);
        std::cout << "number of poses: " << str << std::endl;

        int number_of_poses = boost::lexical_cast<int>(str);

        for(int i=0; i<number_of_poses; ++i)
        {
            wbuf[0] = 'P';
            boost::asio::write(socket, boost::asio::buffer(wbuf), error);
            if (error)
            {
              throw boost::system::system_error(error); 
            }

            boost::array<char, 128> buf;        
            size_t len = socket.read_some(boost::asio::buffer(buf), error);

            if (error == boost::asio::error::eof)
            {
              return 0;
            }
            else if (error)
            {
              throw boost::system::system_error(error); 
            }
            
            std::string str(buf.begin(), buf.begin()+len);
            std::cout << "pose number " << i << ": " << str << std::endl;
            

        }
        

    }catch(std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }

    return 0;
}

